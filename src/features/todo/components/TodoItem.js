import { useTodos } from "../../hooks/useTodo";
import { useState } from "react";
import { EditTwoTone, DeleteTwoTone, EyeTwoTone } from "@ant-design/icons";
import TodoItemModal from "./TodoItemModal";
import TodoItemDetailModal from "./TodoItemDetailModal";

export default function TodoItem({ task }) {
  const [isEditModalOpen, setIsEditModalOpen] = useState(false);
  const [isDetailModalOpen, setIsDetailModalOpen] = useState(false);
  const { updateTodo, deleteTodo } = useTodos();

  const handleTaskNameClick = async () => {
    await updateTodo(task.id, { done: !task.done });
  };

  const hanlleRemoveButtonClick = async () => {
    if (window.confirm("Are you sure you wish to delete this item?")) {
      await deleteTodo(task.id);
    }
  };

  return (
    <div className="todo-item">
      <div
        className={`task-name ${task.done ? "done" : ""}`}
        onClick={handleTaskNameClick}
      >
        {task.name}
      </div>
      <div className="todo-item-buttons">
        <EyeTwoTone
          className="todo-button"
          onClick={() => {
            setIsDetailModalOpen(true);
          }}
        />
        <EditTwoTone
          className="todo-button"
          onClick={() => {
            setIsEditModalOpen(true);
          }}
        />
        <DeleteTwoTone onClick={hanlleRemoveButtonClick} />
      </div>
      <TodoItemModal
        task={task}
        isModalOpen={isEditModalOpen}
        setIsModalOpen={setIsEditModalOpen}
      />
      <TodoItemDetailModal
        taskId={task.id}
        isDetailModalOpen={isDetailModalOpen}
        setIsDetailModalOpen={setIsDetailModalOpen}
      />
    </div>
  );
}
