import { Modal, Input } from "antd";
import { useTodos } from "../../hooks/useTodo";
import { useState } from "react";

export default function TodoItemModal(props) {
  const { TextArea } = Input;
  const { updateTodo } = useTodos();

  const { task, isModalOpen, setIsModalOpen } = props;
  const [newTaskName, setNewTaskName] = useState(task.name);

  const handleOk = async () => {
    await updateTodo(task.id, { name: newTaskName });
    setIsModalOpen(false);
  };

  const handleCancel = () => {
    setIsModalOpen(false);
    setNewTaskName(task.name);
  };

  const onChange = (e) => {
    setNewTaskName(e.target.value);
  };

  return (
    <Modal
      title="Change Task Name"
      open={isModalOpen}
      onOk={handleOk}
      onCancel={handleCancel}
    >
      <TextArea value={newTaskName} onChange={onChange} />
    </Modal>
  );
}
